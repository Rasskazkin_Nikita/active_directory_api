from os import environ
from os.path import dirname, isfile, join
from dotenv import load_dotenv


ROOT_PATH = dirname(__file__)

ENV_PATH = join(ROOT_PATH, '.env')
if isfile(ENV_PATH):
    load_dotenv(ENV_PATH)

LDAP_SERVER = environ.get('LDAP_SERVER')
LDAP_USER = environ.get('LDAP_USER')
LDAP_USER_PASSWORD = environ.get('LDAP_USER_PASSWORD')
LDAP_BASE = environ.get('LDAP_BASE')

if LDAP_SERVER is None:
    raise FileNotFoundError('Setup your .env file')