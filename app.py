from flask import Flask
from flask_restful import Api

from service.api.user import User


app = Flask(__name__)
api = Api(app)

api.add_resource(User, '/api/check/ad-login')

if __name__ == '__main__':

    app.run()
