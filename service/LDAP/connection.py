from ldap3 import Server, ALL
import ldap3

from config import LDAP_SERVER, LDAP_USER, LDAP_USER_PASSWORD, LDAP_BASE


def connect():
    print('Connecting to LDAP servers...', end='')
    server = Server(LDAP_SERVER, get_info=ALL, connect_timeout=10)
    _connection = ldap3.Connection(
        server,
        user=f'cn={LDAP_USER},{LDAP_BASE}',
        password=LDAP_USER_PASSWORD
    )
    server.connect_timeout = 1
    print('\rConnecting to LDAP servers - success')
    _connection.bind()
    return _connection


connection = connect()
