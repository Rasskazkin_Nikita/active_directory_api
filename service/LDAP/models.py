class Base:
    @staticmethod
    def _get_attribute(entry, attribute_name: str):
        if attribute_name in entry:
            return entry[attribute_name].value
        else:
            return None

    @staticmethod
    def _dec_to_bin(number: int, bit_count: int = 28):
        bin_array = []
        for _ in range(bit_count):
            bin_array.insert(0, number % 2)
            number //= 2
        return reversed(bin_array)

    def get_dict_info(self):
        return {}


class User(Base):
    def __init__(self, entry):
        self.user_name = self._get_attribute(entry, 'name')
        self.account_name = self._get_attribute(entry, 'sAMAccountName')
        self.groups = self._get_groups_names(self._get_attribute(entry, 'memberOf'))
        self.status = self._get_status(self._get_attribute(entry, 'userAccountControl'))
        self.enable = True if self.status['NORMAL_ACCOUNT'] == 1 else False

    @staticmethod
    def _get_groups_names(entry):
        groups = []
        if entry is None:
            return groups
        for group in entry:
            group_name, _, dc1, dc2, dc3 = group.split(',')
            groups.append({
                'name': group_name[3:],
                'domain': '.'.join([item[3:] for item in [dc1, dc2, dc3]])  # TODO Исправить список dc
            })
        return groups

    def _get_status(self, entry):
        if entry is None:
            return dict()
        fields = [
            'SCRIPT',
            'ACCOUNTDISABLE',
            None,
            'HOMEDIR_REQUIRED',
            'LOCKOUT',
            'PASSWD_NOTREQD',
            'PASSWD_CANT_CHANGE',
            'ENCRYPTED_TEXT_PWD_ALLOWED',
            'TEMP_DUPLICATE_ACCOUN',
            'NORMAL_ACCOUNT',
            None,
            'INTERDOMAIN_TRUST_ACCOUNT',
            'WORKSTATION_TRUST_ACCOUNT',
            'SERVER_TRUST_ACCOUNT',
            None,
            None,
            'DONT_EXPIRE_PASSWORD',
            'MNS_LOGON_ACCOUNT',
            'SMARTCARD_REQUIRED',
            'TRUSTED_FOR_DELEGATION',
            'NOT_DELEGATED',
            'USE_DES_KEY_ONLY',
            'DONT_REQ_PREAUTH',
            'PASSWORD_EXPIRED',
            'TRUSTED_TO_AUTH_FOR_DELEGATION',
            None,
            'PARTIAL_SECRETS_ACCOUNT',
            None
        ]
        status = dict([
            item
            for index, item in enumerate(zip(fields, self._dec_to_bin(int(entry))))
            if item[0] is not None
        ])
        return status

    def get_dict_info(self):
        return {
            'user_name': self.user_name,
            'account_name': self.account_name,
            'groups': self.groups,
            'enable': self.enable
        }


if __name__ == '__main__':
    base = Base()
