from ldap3.core.exceptions import LDAPSocketReceiveError, LDAPSocketOpenError

from service.LDAP.connection import connection
from service.LDAP.models import *
from service.exeptions import InvalidEntryError
from service.exeptions import LdapServiceConnectionError
from config import LDAP_BASE


class BaseQuerySet:
    @staticmethod
    def search(*args, **kwargs):
        try:
            connection.search(*args, **kwargs)
            return connection.entries
        except LDAPSocketReceiveError:
            raise LdapServiceConnectionError


class UserQuerySet(BaseQuerySet):
    @classmethod
    def get_users(cls, names: list = None):
        if names is None:
            names = ['*']
        attributes = [
            'userAccountControl',
            'memberOf',
            'name',
            'sAMAccountName'
        ]
        names_filter_line = ''.join([f'(sAMAccountName={name})'for name in names])
        entries = cls.search(
            f'cn=users,{LDAP_BASE}',
            f'(&(objectclass=person)(|{names_filter_line}))',
            attributes=attributes
        )
        users = []
        for entry in entries:
            users.append(User(entry))
        if not users:
            raise InvalidEntryError
        return users
