from werkzeug import exceptions


class InvalidBodyError(exceptions.UnprocessableEntity):
    description = 'Invalid request body'


class InvalidEntryError(exceptions.NotFound):
    description = 'Information on the entered data was not found',


class LdapServiceConnectionError(exceptions.ServiceUnavailable):
    description = 'Failed to establish connection to LDAP server'
