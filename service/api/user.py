from flask import request
from flask_restful import Resource

from service.validation.user import validate_post
from service.LDAP.repository import get_user
from service.responses import success_post_response


class User(Resource):
    @staticmethod
    def post():
        body = validate_post(request.get_json())
        data = get_user(body['account_name'])
        return success_post_response(data)
