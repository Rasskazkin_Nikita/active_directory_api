from jsonschema import validate
from jsonschema.exceptions import ValidationError

from service.validation.schemas import USER_POST
from service.exeptions import InvalidBodyError


def validate_post(body):
    try:
        validate(body, USER_POST)
        return body
    except ValidationError:
        raise InvalidBodyError
