USER_POST = {
    'type': 'object',
    'properties': {
        'account_name': {'type': 'string'}
    },
    'required': ['account_name']
}
