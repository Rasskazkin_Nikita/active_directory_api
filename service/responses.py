def success_post_response(data: dict):
    return {
        'message': 'Success',
        'data': data
    }
