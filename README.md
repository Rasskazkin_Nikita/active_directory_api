#Active Directory API

API для получения информации о пользователях из Active Directory

## Развёртывание

Перед развёртыванием сервиса необходимо сконфигурировать ```.env``` файл
### Docker

Для развёртывания сервиса необходим Docker версии 20.10.2 или выше. 
Наиболее удобным способом запуска является использование docker-compose.
```
docker-compose up -d
```

## Методы

### Получение информации о пользователе

```/api/check/ad-login```

#### POST
##### Headers
```Content-Type: application/json```
##### Body
```
{
    "account_name": {String}
}
```
- account_name - имя пользователя при входе в аккаунт, информация о котором будет получена

##### Response

```
{
    "message": "Success",
    "data": {
        "user_name": {String},
        "account_name": {String},
        "groups": [
            {
                "name": {String: group name},
                "domain": {String: group domain}
            }
        ],
        "enable": {Boollean}
    }
}
```

- user_name - полное имя пользователя
- account_name - имя аккаунта для входа
- groups - группы в которых состоит пользователь
- enable - активна ли учётная запись (True, если активна)
