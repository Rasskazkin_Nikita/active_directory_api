import pytest

from tests.base import get_tests
from app import app


@pytest.fixture()
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client


class TestApi:
    @staticmethod
    def test_user_post_negative(client):
        for test in get_tests('user', 'post', 'negative'):
            response = client.post(test['url'], json=test['body'])
            assert response.status_code == test['response']

    @staticmethod
    def test_user_post_positive(client):
        for test in get_tests('user', 'post', 'positive'):
            response = client.post(test['url'], json=test['body'])
            assert response.status_code == 200
