import json


def get_tests(tests_group: str, method: str, test_mod: str):
    with open(f'test_cases/{tests_group}/{method}/{test_mod}.json') as test_file:
        for test in json.load(test_file):
            yield test


if __name__ == '__main__':
    x = next(get_tests('user', 'post', 'negative'))
    pass
