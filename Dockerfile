FROM python:3.9-alpine

RUN adduser -D admin

WORKDIR /home/app

COPY . .

RUN python -m pip install --no-cache-dir --upgrade pip
RUN python -m pip install --no-cache-dir -r requirements.txt
RUN python -m pip install --no-cache-dir gunicorn

RUN chown -R admin:admin ./
USER admin

EXPOSE 5000 389

CMD ["gunicorn", "-b", ":5000", "--access-logfile", "-", "--error-logfile", "-", "app:app"]
